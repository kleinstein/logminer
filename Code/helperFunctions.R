#################################################################################
## helperFunctions.R                                                           ##
#################################################################################

################################################################################
##' mgsub
##' Function to perform multiple gsub operations
##'
##' A wrapper of gsub for multiple strings at a time
##' @title mgsub
##' @param patterns see \code{gsub}
##' @param replacements see \code{gsub}
##' @param strs one or more strings
##' @param ... additional parameters passed to \code{gsub}
##' @return modified version of strs with strings in patterns replaced by corresponding strings in replacements
##' @author  Stefan Philippe Avey
mgsub <- function(patterns, replacements, strs, ...) {
  if( (length(patterns) <= 0) || length(patterns) != length(replacements) ) {
    stop("Must specify the same number of patterns as replacements")
  }
  for(i in seq_along(patterns)) {
    strs <- gsub(patterns[i], replacements[i], strs, ...)
  }
  return(strs)
}

################################################################################
##' FilterTime
##'
##' Filter time and take FC if appropriate between time \code{tp} and time 0
##'
##' 
##' @param eDat expression data frame
##' @param tDat Target data frame corresponding to expression data frame (rows are in same order as columns of eDat) containing columns named `SubjectID` and `Time`
##' @param tp time point to use
##' @param FoldChange Whether or not to calculate a fold change (difference in log2 scale) between \code{tp} and time 0. Default is \code{TRUE}.
##' @return a list containing the new expression matrix in `newEDat` and the new target in `newTDat`
FilterTime <- function(eDat, tDat, tp, FoldChange=TRUE)  {
  if(tp == 0) {
    newEDat <- eDat[,(tDat$Time == tp)]
    newTDat <- tDat[(tDat$Time == tp),]
  } else {
    ## Only keep subjects who have both time points
    subj <- intersect(tDat[tDat$Time == tp, "SubjectID"],
                      tDat[tDat$Time == 0, "SubjectID"])
    newEDat <- eDat[,(tDat$Time == tp & tDat$SubjectID %in% subj)] -
      eDat[,(tDat$Time == 0 & tDat$SubjectID %in% subj)]
    newTDat <- tDat[(tDat$Time == tp) & (tDat$SubjectID %in% subj), ]
  }
  return(list(newEDat=newEDat, newTDat=newTDat))
}

################################################################################
##' FilterFeatures
##'
##' Initial Variable selection based on specified criteria
##'
##' The criteria passed in is used to filter the features in the data
##' 
##' @param x A matrix (or data.frame) with features in rows and samples in columns. Row names will be used for the return value.
##' @param top a positive integer specifying how many features to select from x
##' @param method which method to use for filtering. One of POS, CV, or COR. CAUTION: Only CV and AvgAb should be used outside of Cross Validation.  The other methods, if used should be used inside cross validation.
##' CV - Use highest coefficient of variation
##' POS - Use proportional Overlapping Score (requires propOverlap R package)
##' COR - Filter out bottom `perc` by CV, then use highest correlation with y
##' AvgAb - Take the average of the absolute value. Appropriate for fold changes.
##' @param covar a covariate that might effect variable selection. When response is confounded by another variable such as cohort or gender, these may be passed in as covariates and the genes will be chosen in each group separately and then combined. Currently only 1 is allowed as a factor of length equal to number of columns in \code{x}. Results will not be that meaningful if groups of covariates are very small and might break the function if no variation exists in genes within groups of covariates. Default is \code{NULL}. NOT SURE HOW USEFUL THIS ARGUMENT IS (EXPERIMENTAL ONLY).
##' @param y the response variable. Not needed for CV.
##' POS - A factor, see Sel.Features function in propOverlap
##' COR - values (numeric) to correlate with x
##' @param perc quantile used for filtering by CV when COR is chosen. Default 0.75 means that anything less than the top 75% in the CV distribution is filtered out before correlation is measured.
##' @return a vector of the features (given based on the row names of x)
##' @details 
FilterFeatures <- function(x, top, method=c("CV", "POS", "COR", "AvgAb"), covar=NULL, y=NULL, perc=0.75) {
  match.arg(method)
  if( ((method == "POS") || (method == "COR")) && is.null(y)) {
    stop("`y` must be specified when method is `POS` or `COR`")
  }
  if( !is.numeric(top) && (top > 0) && (top == round(top, digits=0)) ) {
    stop("`top` must be a positive integer")
  }
  if( !is.null(covar) && ((method == "POS") || (method == "COR")) ) {
    stop("Sorry, haven't implemented covariates for methods `POS` or `COR`.")
  }
  if( !is.null(covar) && (length(covar) != ncol(x) || !is.factor(covar)) ) {
    stop("`covar` must be a vector of factors of length ncol(x).")
  }
  switch(method,
         "CV" = {       # Use Coefficient of variation to rank the genes
           if(is.null(covar)) {
             cv <- apply(as.matrix(2^x), 1, function(row) sd(row)/mean(row))
             cvSorted <- sort(cv, decreasing=TRUE)
             feats <- names(cvSorted[1:top])
           } else {
             ## Create matrix of rankings (one column for each cov in covar)
             cvMat <- vapply(levels(covar), function(cov)
                             names(sort(apply(as.matrix(x[,covar == cov]), 1,
                                              function(row) sd(row)/mean(row)), decreasing=TRUE)),
                             FUN.VALUE=character(length=nrow(x)))
             ## tranpose so that vector goes by row in original matrix
             ranking <- as.vector(t(cvMat))
             ## Remove duplicates from the ranking
             ranking <- ranking[!duplicated(ranking)]
             feats <- ranking[1:top]
           }
         },
         "AvgAb" = {       # Use average absolute value to rank features
           if(is.null(covar)) {
             aa <- apply(as.matrix(x), 1, function(row) mean(abs(row)))
             aaSorted <- sort(aa, decreasing=TRUE)
             feats <- names(aaSorted[1:top])
           } else {
             ## Create matrix of rankings (one column for each cov in covar)
             aaMat <- vapply(levels(covar), function(cov)
                             names(sort(apply(as.matrix(x[,covar == cov]), 1,
                                              function(row) mean(abs(row))), decreasing=TRUE)),
                             FUN.VALUE=character(length=nrow(x)))
             ## tranpose so that vector goes by row in original matrix
             ranking <- as.vector(t(aaMat))
             ## Remove duplicates from the ranking
             ranking <- ranking[!duplicated(ranking)]
             feats <- ranking[1:top]
           }
         },
         "POS" = { # Use Proportional Overlapping Score (POS) to rank the genes
           if(require(propOverlap)) {
             sf <- Sel.Features(as.matrix(x), y, K=top, Verbose=TRUE)
             ord <- order(sf$Measures$Pos, decreasing=FALSE) # sort by most to least discriminative
             feats <- rownames(x)[sf$Features[ord]]
           }
         },
         "COR" = { # Use Correlation with actual respType
           ## cutoff <- 6.75
           ## Filter by minimum expression
           ## filter <- apply(as.matrix(x), 1, function(row)
           ##                 mean(row) > cutoff)
           ##
           ## Filter out bottom 75% by CV, then take `top` genes with highest correlation
           cv <- apply(as.matrix(x), 1, function(row) sd(row)/mean(row))
           filter <- cv > quantile(cv, perc)
           cors <- apply(as.matrix(x[filter,]), 1, function(row)
                         cor(row, y))
           ord <- order(abs(cors), decreasing=TRUE) # sort by abs(correlation)
           feats <- rownames(x)[filter][ord][1:top]
         }
         )
  return(feats)
}

################################################################################
##' CreateFolds
##'
##' Create Folds for Cross Validation
##'
##' Divide the observations up into (balanced) folds. The function tries to split up the factors given in allGroups to be approximately equal number in each fold
##' 
##' @param fold The number of folds. -1 for leave one out CV (LOOCV)
##' @param groups A vector of factors specifying the group each sample belongs to (e.g. combinations of year, batch, age, like 'year1.batch1.oldGroup'). Should be the same length as the number of observations
##' @param allGroups All possible combinations of the groups
##' @return a list of vectors specifying the observations in each fold (each list element corresponds to 1 fold)
CreateFolds <- function(fold, groups, allGroups) {
  fold <- as.integer(fold)              # must be an integer
  if( (fold > 0) && (fold < length(groups)) ) {
    ## Create balanced groups
    foldl <- vector(mode="list", length=fold)
    curFold <- 1
    for(lev in levels(groups)) {
      subGroup <- which(groups == lev)
      ## sample(x) has special property if x is length 1 that I don't want see ?sample
      if(length(subGroup) > 1) { 
        subGroup <- sample(subGroup) # sample() ensures randomness of partitions in different runs
      }
      for(sub in subGroup) {
        foldl[[curFold]] <- c(foldl[[curFold]], sub)
        curFold <- (curFold %% fold) + 1 # keep cycling, i.e. {1, 2, ... fold, 1, 2, ..., fold, ...}
      }
    }
  } else if( (fold == -1) || (fold == length(groups)) ) {          # LOOCV
    foldl <- as.list(1:sum(train)) # each subject is in their own 'fold'
  } else {
    stop("Fold = ", fold, "\nlength(groups) = ", length(groups), "\n",
         "Fold must be an integer in {-1, 1, 2, ..., n] ",
         "where n == length(groups) is the number of observations.")
  }
  names(foldl) <- 1:length(foldl)
  return(foldl)
}

################################################################################
##' Accuracy

Accuracy <- function(table) {
  ## if not square or not a table object, stop
  if( (dim(table)[1] != dim(table)[2]) || !is.table(table) )
    stop("Table must have same number of rows and columns.")
  if(all(c("0", "1") %in% rownames(table))) {
    pos <- "1"; neg <- "0"
  } else if(all(c("R", "NR") %in% rownames(table))) {
    pos <- "R"; neg <- "NR"
  } else
    stop("rownames are not 0/1 or R/NR/etc.")
  TP <- table[pos,pos] # sum of true positives
  TN <- table[neg,neg] # sum of true negatives
  return((TP + TN) / sum(table))
}

################################################################################
##' Sensitivity

Sensitivity <- function(table) {
  ## if not square or not a table object, stop
  if( (dim(table)[1] != dim(table)[2]) || !is.table(table) )
    stop("Table must have same number of rows and columns.")
  if(all(c("0", "1") %in% rownames(table))) {
    pos <- "1"
  } else if(all(c("R", "NR") %in% rownames(table))) {
    pos <- "R"
  } else
    stop("rownames are not 0/1 or R/NR/etc.")
  if(names(dimnames(table))[1] == "predicted") {
    CP <- sum(table[,pos]) # sum of condition positive
  } else if(names(dimnames(table))[1] == "true") {
    CP <- sum(table[pos,]) # sum of condition positive
  } else {
    stop("Dimmension names of the table must be 'predicted' and 'true'.")
  }
  TP <- table[pos,pos] # sum of true positives
  return(TP / CP)
}

################################################################################
##' Specificity

Specificity <- function(table) {
  ## if not square or not a table object, stop
  if( (dim(table)[1] != dim(table)[2]) || !is.table(table) )
    stop("Table must have same number of rows and columns.")
  if(all(c("0", "1") %in% rownames(table))) {
    neg <- "0"
  } else if(all(c("R", "NR") %in% rownames(table))) {
    neg <- "NR"
  } else
    stop("rownames are not 0/1 or R/NR/etc.")
  if(names(dimnames(table))[1] == "predicted") {
    CN <- sum(table[,neg]) # sum of condition negative
  } else if(names(dimnames(table))[1] == "true") {
    CN <- sum(table[neg,]) # sum of condition negative
  } else {
    stop("Dimmension names of the table must be 'predicted' and 'true'.")
  }
  TN <- table[neg,neg] # sum of true negatives
  return(TN / CN)
}

######################################################################################
## CreateNetworks                                                                   ##
######################################################################################
##' Create the networks from network names and a saved R Data file.
##'
##' Method 'subgraph' will first add any nodeNames from \code{nodeNames} not in
##' each network and then filter the network (with \code{igraph::induced.subgraph}).
##' Method 'connected.subgraph' will take the ranked names from \code{nodeNames} and
##' add one gene at a time (from the top of \code{nodeNames}) until a connected
##' subgraph with at least \code{networkSize} nodes is created.
##' NOTE: The final graph may have more than \code{networkSize} nodes if method is
##' 'connected.subgraph.'
##' @title CreateNetworks
##' @param nodeNames A character vector giving the names of the nodes that should be in each network. If any of these names are not already in the network, they are added as unconnected nodes. Any nodes in the network but not in nodeNames will be filtered out of the network. Must be ranked in order from first to last if method is 'connected.subgraph'
##' @param networkFile The (full) filename path to an R Data Set (.rds) containing the networks in igraph format. Default is \code{NULL} but must be specified unless simNets is \code{TRUE}.
##' @param netNames A named character vector giving the names of the objects in networkFile to include in the returned list of networks AND the the names of the list (as names in the character vector).
##' @param simNets if FALSE (default), real networks (not simulated) are used. Otherwise, it must be a numeric vector, starting with 0 as the first element, specifying the density of each of the random networks with length > 1.
##' @param invert if TRUE, weights on the edges in the network will be replaced by \code{1 - weights}. Assumes that all weights are between 0 and 1 (otherwise this will create negative weights). Default is FALSE.
##' @param permute logical indicating whether to return permuted versions of real networks in addition to the real graphs. Graphs named "Unconnected" or "Connected" will not be permuted as that would result in the same exact graph for a fully unconnected or connected graph.
##' @param unConFlag if TRUE (default), an unconnected network named 'Unconnected' with nodeNames and no edges is created and returned
##' @return A named list of igraph graph objects.
##' @import igraph
##' @author Stefan Avey
CreateNetworks <- function(nodeNames, networkFile=NULL, netNames=NULL, networkSize=NULL,
                           method=c("subgraph", "connected.subgraph"),
                           simNets=FALSE, invert=FALSE, permute=FALSE, unConFlag=TRUE) {
  ## Check for conditions that would cause a failure
  method <- match.arg(method)
  if (method == "connected.subgraph") {
    if(is.null(networkSize)) {
      stop("networkSize must be non-NULL when method == 'connected.subgraph'")
    }
    if(simNets) {
      stop("Cannot simulate networks when method == 'connected.subgraph'")
    }
  } else {
    networkSize <- length(nodeNames)
  }
  if(!unConFlag && simNets) {
    stop("When simNets=TRUE, unConFlag must be TRUE")
  }
  if(invert && permute) {
    stop("Only invert or permute (but not both) can be TRUE simultaneously")
  }
  if(invert) {
    warning("Invert code is not well tested and could produce erroneous results")
  }
  
  ## Create unconnected network
  unCon <- graph.empty(networkSize, directed=FALSE)
  V(unCon)$name <- nodeNames[1:networkSize]

  ## If simulating networks of different density, create them from the Unconnected network
  if(length(simNets) > 1) {
    if(!is.null(netNames) || !is.null(networkFile)) {
      warning("Ignoring netNames and networkFile since simNets is TRUE")
    }
    densities <- simNets
    gList <- vector(mode="list", length=length(densities))
    names(gList) <- as.character(densities)
    gList[[1]] <- unCon
    ## Create new random graphs with the desired densities
    for(d in seq_along(densities)[-1]) {     # loop through densities skipping the first index, 1
      numEdges <- round(choose(networkSize, 2)*densities[d]) # number of total edges new graph should have
      ## ## Create home-made random graphs (but these don't follow power law distribution)
      ## prevGraphEdgeNum <- ifelse(densities[d-1] == 0, 0, length(E(gList[[d-1]])))
      ## gList[[d]] <- gList[[d-1]] + edges(sample(x=nodeNames,
      ##                                           size=(numEdges*2) - (prevGraphEdgeNum*2),
      ##                                           replace=TRUE))
      ## Create Erdos-Renyi random graphs
      gList[[d]] <- erdos.renyi.game(n=networkSize, p.or.m=numEdges,
                                     type="gnm", directed=FALSE)
      V(gList[[d]])$name <- nodeNames
    }
  } else {
    if(is.null(netNames)) {
      stop("netNames must be specified when simNets is FALSE")
    }
    ## Read in networks from a file (object is called gList)
    if(file.exists(networkFile)) {
      gList <- readRDS(networkFile)
      gList <- gList[netNames]          # select only appropriate names
      names(gList) <- names(netNames)
    } else {
      stop("File: ", networkFile, " not found") 
    }
    if(unConFlag) {
      gList <- c(gList, list(Unconnected=unCon))
    }
  }

  ## Create networks differently depending on the method
  if(method == "subgraph") {
    ## Must add genes not in the networks as unconnected nodes
    agList <- lapply(gList, function(graph)
                     {
                       newNodes <- setdiff(nodeNames, V(graph)$name)
                       igraph::add.vertices(graph, length(newNodes), attr=list(name=newNodes))
                     })

    ## Refine the network based on the genes selected and add unconnected genes into
    ## the network so that all networks are the same size!
    sgList <- lapply(agList, function(graph) induced.subgraph(graph, nodeNames))
  } else if (method == "connected.subgraph") {
    if(is.null(networkSize)) {
      stop("networkSize must be non-NULL when method == 'connected.subgraph'")
    }
    sgList <- list()
    realNets <- grep("unconnected", names(gList), ignore.case=TRUE, invert=TRUE, value=TRUE)
    for(net in realNets) {
      numFeats <- networkSize - 1
      maxMembers <- c()                       # empty vector
      graph <- gList[[net]]
      ## Remove any components that don't meet the size requirement
      cgraph <- clusters(graph)
      tooSmall <- which(cgraph$csize < networkSize)
      toKeep <- setdiff(1:length(V(graph)), which(cgraph$membership %in% tooSmall))
      graph <- induced.subgraph(graph, vids=toKeep)
      tempFeats <- feats[feats %in% V(graph)$name]
      ## Create sgList
      while(length(maxMembers) < networkSize) {
        numFeats <- numFeats + 1
        gr <- induced.subgraph(graph, vids=tempFeats[1:numFeats])
        cgr <- clusters(gr)
        maxClust <- which.max(cgr$csize)
        maxMembers <- which(cgr$membership == maxClust)
      }
      sgList[[net]] <- induced.subgraph(gr, vids=maxMembers)
      if(!is.connected(sgList[[net]])) {
        stop("graph ", net, " is not connected but should be!")
      }
    }
    sgList <- c(sgList, list(Unconnected=unCon))
  } else {
    stop("A valid method must be supplied")
  }
  ## 'Invert' the network so that weights get 1-weights since all weights are between 0 and 1
  ## unconnected nodes become connected with weight 0 first, and then will be connected with
  ## weight 1 in the 'inverted' network.
  if(invert) {
    if(length(simNets) > 1) {
      stop("Will not invert simulated (random) networks")
    }
    con <- graph.full(length(nodeNames))
    V(con)$name <- nodeNames
    E(con)$weight <- 1 # weight all edges with 1 in the connected network
    edgeListCon <- data.frame(get.edgelist(con) , weight=E(con)$weight)
    ## fake are the fully (un)connected, real are other biologically meaningful networks  
    realNets <- grep('connected', names(sgList), ignore.case=TRUE, invert=TRUE, value=TRUE) 
    sgList2 <- list()
    ## allNewNodes <- c()
    for(realNet in realNets) {
      graph <- sgList[[realNet]]
      if(!is.weighted(graph)) { E(graph)$weight <- 1 }
      edgeList <- data.frame(get.edgelist(graph) , weight=E(graph)$weight)
      el1 <- edgeList[,1:3]
      el2 <- edgeList[,c(2,1,3)] # need 2 because order might be swapped in edgeListCon
      colnames(el2) <- colnames(el1)
      tmp <- merge(edgeListCon[,1:2], rbind(el1, el2), all.x=TRUE, sort=FALSE)
      tmp$weight[is.na(tmp$weight)] <- 0  # fill in 0's for NA values
      ## Invert weights by using 1 - weight
      tmp$weight <- 1 - tmp$weight
      ## Remove any edges that are now 0-weighted
      tmp <- tmp[tmp$weight != 0,]
      ## ## Remove any edges between genes not in the original network ##
      ## ## Decided not to remove these but to completely invert the network ##
      ## newNodes <- setdiff(nodeNames, V(gList[[realNet]])$name)
      ## ## allNewNodes <- c(allNewNodes, newNodes)
      ## tmp <- tmp[!((tmp$X1 %in% newNodes) | (tmp$X2 %in% newNodes)),]
      ## Store new network in a list
      sgList2[[realNet]] <- graph.data.frame(tmp, directed=FALSE)
      ## ## Add back the Unconnected vertices
      ## sgList2[[realNet]] <- igraph::add.vertices(sgList2[[realNet]],
      ##                                    length(newNodes),
      ##                                    attr=list(name=newNodes))
    }
    names(sgList2) <- paste(names(sgList2), "Inverted", sep="_")
    sgList <- c(sgList, sgList2)
    ## sort(unique(allNewNodes))
    if(!interactive()) {
      rm(con, sgList2, tmp, edgeListCon, edgeList, el1, el2) # free up some memory
    }
  } else if(permute) {
    ## Permute the gene labels in the network (using `sample` function)
    sel <- setdiff(names(sgList), c("Unconnected", "Connected"))
    sgListRand <- lapply(sgList[sel], function(graph)
                         {
                           graphPerm <- graph
                           V(graphPerm)$name <- sample(V(graph)$name)                             
                           return(graphPerm)
                         })
    names(sgListRand) <- paste(names(sgListRand), "Permuted", sep="_")
    sgList <- c(sgList, sgListRand)
  }
  return(sgList)
}

######################################################################################
##' printMissed
##' 
##' @param l a named list containing the `predicted` and `true` values
##' @param subset numeric indices giving the relelvant rows of \code{dat} that
##'               elements in \code{l} correspond to 
##' @param dat a data.frame or matrix containing the columns in \code{cols} and rows
##'            given by \code{subset}
##' @param title an optional title to print out first
##' @param cols the column names or indices corresponding to \code{dat}
##' @param showCorrect if TRUE (default) the correct observations will be shown as
##'                    well as the incorrect ones.
printMissed <- function(l, subset, dat, title=NULL,
                        cols=1:ncol(dat), showCorrect=TRUE) {
  if(!is.null(title)) {
    cat(title, ":\n", sep='')
  }
  ne <- l$predicted != l$true
  missed <- subset[ne]
  cat("  - Misclassified (", length(missed), ")\n", sep='')
  if(length(missed)) {
    out <- dat[missed, cols]
    rownames(out) <- NULL
    out <- cbind(out, True=l$true[ne], Predicted=l$predicted[ne])
    print(out)
  }
  if(showCorrect) {
    correct <- setdiff(subset, missed)
    cat("  - Correctly Classified (", length(correct), ")\n", sep='')
    if(length(correct)) {
      out <- dat[correct, cols]
      rownames(out) <- NULL
      ## out <- cbind(out, True=l$true[!ne], Predicted=l$predicted[!ne])      
      print(out)
    }
  }
  cat('\n')
}

######################################################################################
##' printConfusionResults
##' 
##' @param conf a confusion matrix
printConfusionResults <- function(conf) {
  print(conf)
  cat("Sensitivity:", Sensitivity(conf), '\n')
  cat("Specificity:", Specificity(conf), '\n')
  cat("Accuracy:", Accuracy(conf), '\n')
}

################################################################################
##' cv.pclogit
##'
##' Cross validation function to optimize lambda and alpha in pclogit
##'
##' This is modeled very closely off of cv.glmnet from the glmnet package.
##' 
##' @param x A matrix (or data.frame) with features in columns and observations in rows.
##' @param y the continuous response variable with observations in rows.
##' @param lambda Optional user-supplied lambda sequence; default is 'NULL',
##'        and 'glmnet' chooses its own sequence
##' @param weights Observation weights; defaults to 1 per observation
##' @param offset Offset vector (matrix) as in 'glmnet'
##' @param corDir Should the correlation between features and response
##'        (done repeatedly in each fold) be used to specify the \code{sgnc}
##'        option in \code{pclogit}? Default is FALSE. Should not be set to
##'        TRUE when the number of observations in each fold is small
##'        (less than or equal to 3)
##' @param type.measure loss to use for cross-validation.
##'        See \code{cv.glmnet} for more details
##' @param nfolds number of folds - default is 10. Although 'nfolds' can be as
##'        large as the sample size (leave-one-out CV), it is not
##'        recommended for large datasets. Smallest value allowable is 'nfolds=3'
##' @param foldid an optional vector of values between 1 and 'nfold'
##'        identifying what fold each observation is in. If supplied,
##'        'nfold' can be missing.
##' @param grouped This is an experimental argument, with default 'TRUE', and
##'        can be ignored by most users. See \code{cv.glmnet} for more details.
##' @param keep If 'keep=TRUE', a _prevalidated_ array is returned containing
##'        fitted values for each observation and each value of
##'        'lambda'. This means these fits are computed with this
##'        observation and the rest of its fold omitted. The 'folid'
##'        vector is also returned. Default is keep=FALSE
##' @param parallel If 'TRUE', use parallel 'foreach' to fit each fold.  Must
##'        register parallel before hand, such as 'doMC' or others.  See
##'        the example in \code{cv.glmnet}
##' @param ... Other arguments passed to \code{pclogit} function
cv.pclogit <- function (x, y, lambda = NULL, weights, offset = NULL, corDir = FALSE,
                        type.measure = c("mse", "deviance", "class", "auc", "mae"),
                        nfolds = 10, foldid, grouped = TRUE,
                        keep = FALSE, parallel = FALSE, ...) {
  if (missing(type.measure)) 
    type.measure = "default"
  else type.measure = match.arg(type.measure)
  if (!is.null(lambda) && length(lambda) < 2) 
    stop("Need more than one value of lambda for cv.pclogit")
  N = nrow(x)
  if (missing(weights)) 
    weights = rep(1, N)
  else weights = as.double(weights)
  y = drop(y)
  pclogit.call = match.call(expand.dots = TRUE)
  which = match(c("type.measure", "nfolds", "foldid", "grouped", 
      "keep"), names(pclogit.call), F)
  if (any(which)) 
    pclogit.call = pclogit.call[-which]
  pclogit.call[[1]] = as.name("pclogit")
  if(corDir) {
    sgn <- as.numeric(sign(cor(x, as.numeric(y)-1)))
  } else {
      sgn <- NULL
    }
  pclogit.object = pclogit(x, as.numeric(y)-1, lambda = lambda,
      sgnc = sgn, ...)
  pclogit.object$call = pclogit.call
  pclogit.object$classnames = c(0,1)
  pclogit.object$family = "binomial"
  pclogit.object$offset = FALSE
  pclogit.object$a0 = pclogit.object$b0        
  class(pclogit.object) <- c("lognet", "glmnet")
  if (inherits(pclogit.object, "multnet") && !pclogit.object$grouped) {
    nz = predict(pclogit.object, type = "nonzero")
    nz = sapply(nz, function(x) sapply(x, length))
    nz = ceiling(apply(nz, 1, median))
  }
  else nz = sapply(predict(pclogit.object, type = "nonzero"), 
           length)
  if (missing(foldid)) 
    foldid = sample(rep(seq(nfolds), length = N))
  else nfolds = max(foldid)
  if (nfolds < 3) 
    stop("nfolds must be bigger than 3; nfolds=10 recommended")
  if ( (floor(N / nfolds) <= 3) && corDir )
    stop("'corDir' cannot be TRUE without more than 3 observations per fold")
  outlist = as.list(seq(nfolds))
  if (parallel) {
    outlist = foreach(i = seq(nfolds), .packages = c("pclogit")) %dopar% 
{
  which = foldid == i
  if (is.matrix(y)) 
    y_sub = y[!which, ]
  else y_sub = y[!which]
  if(corDir) {
    sgn <- as.numeric(sign(cor(x[!which, , drop = FALSE],
                               as.numeric(y_sub)-1)))
    
  } else {
      sgn <- NULL
    }
  pcOut <- pclogit(x[!which, , drop = FALSE], as.numeric(y_sub)-1,
                   lambda = lambda, sgnc = sgn, ...)
  pcOut$call = pclogit.call
  pcOut$classnames = c(0,1)
  pcOut$family = "binomial"
  pcOut$offset = FALSE
  pcOut$a0 = pcOut$b0        
  class(pcOut) <- c("lognet", "glmnet")
  return(pcOut)
}
} else {
for (i in seq(nfolds)) {
  which = foldid == i
  if (is.matrix(y)) 
    y_sub = y[!which, ]
  else y_sub = y[!which]
  if(corDir) {
    sgn <- as.numeric(sign(cor(x[!which, , drop = FALSE],
                               as.numeric(y_sub)-1)))
    
  } else {
      sgn <- NULL
    }
  pcOut <- pclogit(x[!which, , drop = FALSE], 
                   as.numeric(y_sub)-1, lambda = lambda,
                   sgnc = sgn, ...)
  pcOut$call = pclogit.call
  pcOut$classnames = c(0,1)
  pcOut$family = "binomial"
  pcOut$offset = FALSE
  pcOut$a0 = pcOut$b0        
  class(pcOut) <- c("lognet", "glmnet")
  outlist[[i]] = pcOut
}
}
fun = paste("cv", class(pclogit.object)[[1]], sep = ".")
lambda = pclogit.object$lambda
cvstuff = do.call(fun, list(outlist = outlist,
    offset = offset, weights = weights, lambda = lambda, x = x, y = y,
    foldid = foldid, type.measure = type.measure, grouped = grouped , keep = keep))
cvm = cvstuff$cvm
cvsd = cvstuff$cvsd
nas = is.na(cvsd)
if (any(nas)) {
lambda = lambda[!nas]
cvm = cvm[!nas]
cvsd = cvsd[!nas]
nz = nz[!nas]
}
cvname = cvstuff$name
out = list(lambda = lambda, cvm = cvm, cvsd = cvsd, cvup = cvm + 
  cvsd, cvlo = cvm - cvsd, nzero = nz, name = cvname, glmnet.fit = pclogit.object)
if (keep) 
  out = c(out, list(fit.preval = cvstuff$fit.preval, foldid = foldid))
lamin = if (cvname == "AUC") 
          getmin(lambda, -cvm, cvsd)
        else getmin(lambda, cvm, cvsd)
obj = c(out, as.list(lamin))
class(obj) = "cv.glmnet"
return(obj)
}
