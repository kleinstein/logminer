####################################################################################
## logminerCORE.R                                                                 ##
## Purpose is to fit L1 logistic regression, elastic net, and network-constrained ##
## models given the training data, testing data, and parameters/flags             ##
####################################################################################

## Create a list to hold all results
resultList <- list()
modelList <- list()
weightList <- list()

############################
## L1 Logistic Regression ##
############################

cat("\n\nBuilding L1-Regularized Logistic Regression Model...\n")

glmnet.cv.result <- cv.glmnet(X_train, as.factor(Y_train), foldid=foldid,
                              alpha=1,  # elastic-net mixing parameter (1=Lasso)
                              ## lambda = lambdas,
                              nlambda = nlam,
                              parallel = parallelFlag,
                              family="binomial", type.measure="class")
ind <- which.min(glmnet.cv.result$cvm)
cvErrorMin <- glmnet.cv.result$cvm[ind]
cvSEMin <- glmnet.cv.result$cvsd[ind]

## Training (Fitted) Confusion Matrix
preds <- predict(glmnet.cv.result, X_train, s=cvMeth, type="class", grouped=FALSE)
lisTrain <- list(predicted=factor(preds, levels=0:1),
                 true=factor(as.numeric(Y_train), levels=0:1))
tabTrain <- table(predicted=lisTrain$predicted, true=lisTrain$true)
if (interactive() || sinkOut) {
  printMissed(l=lisTrain, subset=which(train), dat=targetTrain,
              cols = targetCols,
              title="Training (Fitted) Model")
  printConfusionResults(tabTrain)
}

if (exists("X_test")) {
  ## Testing Confusion Matrix
  preds <- predict(glmnet.cv.result, X_test, s=cvMeth, type="class")
  lisTest <- list(predicted=factor(preds, levels=0:1),
                  true=factor(as.numeric(Y_test), levels=0:1))
  tabTest <- table(predicted=lisTest$predicted, true=lisTest$true)
  if (interactive() || sinkOut) {
    printMissed(l=lisTest, subset=which(test), dat=targetTest,
                cols = targetCols,
                title="Testing Model")
    printConfusionResults(tabTest)  
  }
} else {
    tabTest <- as.table(matrix(rep(NA, 4), nrow=2,
                               dimnames=list(predicted = c("0", "1"),
                                   true = c("0", "1"))))
  }
    

## Store results
numSelGenes <- glmnet.cv.result$glmnet.fit$df[which(glmnet.cv.result[[cvMeth]] == glmnet.cv.result$lambda)]
weights <- glmnet.cv.result$glmnet.fit$beta[,which(glmnet.cv.result[[cvMeth]] ==
                                                       glmnet.cv.result$lambda)]
cvErrorOpt <- glmnet.cv.result$cvm[glmnet.cv.result$lambda ==
                                        glmnet.cv.result[[cvMeth]]]
cvSEOpt <- glmnet.cv.result$cvsd[glmnet.cv.result$lambda ==
                                      glmnet.cv.result[[cvMeth]]]

out <- data.frame(numSelGenes, cvErrorOpt, cvSEOpt, 1, glmnet.cv.result[[cvMeth]],
                  Sensitivity(tabTrain), Specificity(tabTrain), Accuracy(tabTrain),
         Sensitivity(tabTest), Specificity(tabTest), Accuracy(tabTest))
names(out) <-  c("Num Features Selected", "CV Error", "CV SE", "Optimal Alpha", "Optimal Lambda",
                 "Sensitivity (Discovery)", "Specificity (Discovery)", "Accuracy (Discovery)",
                "Sensitivity (Validation)", "Specificity (Validation)", "Accuracy (Validation)")
show(out)
weightList$L1LogReg <- weights
resultList$L1LogReg <- out
modelList$L1LogReg <- glmnet.cv.result

#################
## Elastic Net ##
#################
cat("\n\nBuilding Elastic Net Logistic Regression Model...\n")

cvList <- vector(mode="list", length=length(alphas))
names(cvList) <- as.character(alphas)

for (i in seq_along(alphas)) {
  if (verbose) {
    cat("alpha =", alphas[i], ':\n')
  }
  cvList[[i]] <- cv.glmnet(X_train, as.factor(Y_train), foldid=foldid,
                           alpha=alphas[i],  # elastic-net mixing parameter (1=Lasso, 0 =Ridge)
                           nlambda = nlam, parallel = parallelFlag,
                           family="binomial", type.measure="class")
}

## Find the alpha value that has the minimum cross validation measure
## If multiple alpha values share the same minimum, choose the more sparse one
cvmMat <- Reduce(cbind, lapply(cvList, function(cvRes)
  if (is.null(cvRes)) {
    return(NA)
  } else {
      return(cvRes$cvm)
    }))
cvsdMat <- Reduce(cbind, lapply(cvList, function(cvRes)
  if (is.null(cvRes)) {
    return(NA)
  } else {
      return(cvRes$cvsd)
    }))
allInds <- which(cvmMat == min(cvmMat, na.rm=TRUE), arr.ind=TRUE)
## Include any models within one standard error of the minimum
if (cvMeth == "lambda.1se") {
  se <- min(cvsdMat[allInds], na.rm = TRUE)
  allInds <- which(cvmMat < (min(cvmMat, na.rm=TRUE) + se), arr.ind=TRUE)  
}
## Choose most sparse or least sparse depending on the value of cvOpt
if (cvOpt == "sparse") {
  rind <- nrow(allInds)
} else if (cvOpt == "net") {
  rind <- 1
}
sel <- allInds[rind, "col"]
glmnet.cv.result <- cvList[[sel]]

ind <- which.min(glmnet.cv.result$cvm)
cvErrorMin <- glmnet.cv.result$cvm[ind]
cvSEMin <- glmnet.cv.result$cvsd[ind]

## Training (Fitted) Confusion Matrix
preds <- predict(glmnet.cv.result, X_train, s=cvMeth, type="class", grouped=FALSE)
lisTrain <- list(predicted=factor(preds, levels=0:1),
                 true=factor(as.numeric(Y_train), levels=0:1))
tabTrain <- table(predicted=lisTrain$predicted, true=lisTrain$true)
if (interactive() || sinkOut) {
  printMissed(l=lisTrain, subset=which(train), dat=targetTrain,
              cols = targetCols,
              title="Training (Fitted) Model")
  printConfusionResults(tabTrain)
}

if (exists("X_test")) {
  ## Testing Confusion Matrix
  preds <- predict(glmnet.cv.result, X_test, s=cvMeth, type="class")
  lisTest <- list(predicted=factor(preds, levels=0:1),
                  true=factor(as.numeric(Y_test), levels=0:1))
  tabTest <- table(predicted=lisTest$predicted, true=lisTest$true)
  if (interactive() || sinkOut) {
    printMissed(l=lisTest, subset=which(test), dat=targetTest,
                cols = targetCols,
                title="Testing Model")
    printConfusionResults(tabTest)  
  }
} else {
    tabTest <- as.table(matrix(rep(NA, 4), nrow=2,
                               dimnames=list(predicted = c("0", "1"),
                                   true = c("0", "1"))))
  }

## Store results
numSelGenes <- glmnet.cv.result$glmnet.fit$df[which(glmnet.cv.result[[cvMeth]] == glmnet.cv.result$lambda)]
weights <- glmnet.cv.result$glmnet.fit$beta[,which(glmnet.cv.result[[cvMeth]] ==
                                                       glmnet.cv.result$lambda)]
cvErrorOpt <- glmnet.cv.result$cvm[glmnet.cv.result$lambda ==
                                        glmnet.cv.result[[cvMeth]]]
cvSEOpt <- glmnet.cv.result$cvsd[glmnet.cv.result$lambda ==
                                      glmnet.cv.result[[cvMeth]]]

out <- data.frame(numSelGenes, cvErrorOpt, cvSEOpt, alphas[sel], glmnet.cv.result[[cvMeth]],
                  Sensitivity(tabTrain), Specificity(tabTrain), Accuracy(tabTrain),
                  Sensitivity(tabTest), Specificity(tabTest), Accuracy(tabTest))
names(out) <-  c("Num Features Selected", "CV Error", "CV SE", "Optimal Alpha", "Optimal Lambda",
                 "Sensitivity (Discovery)", "Specificity (Discovery)", "Accuracy (Discovery)",
                "Sensitivity (Validation)", "Specificity (Validation)", "Accuracy (Validation)")
show(out)
weightList$enLogReg <- weights
resultList$enLogReg <- out
modelList$enLogReg <- glmnet.cv.result

##############################################################
## pclogit : L1 and Laplacian penalized logistic regression ##
##############################################################

## Create lists to hold results
wList <- modList <- sapply(names(adjList), function(x) NULL)
## Create data.frame to hold the results
resultTable <- as.data.frame(matrix(NA, nrow=length(adjList), ncol=13))
colnames(resultTable) <- c("Network", "Network Size",
                           "CV Error", "CV SE", "Optimal Alpha", "Optimal Lambda",
                           "Num Features Selected", "Sensitivity (Discovery)",
                           "Specificity (Discovery)", "Accuracy (Discovery)",
                           "Sensitivity (Validation)", "Specificity (Validation)",
                           "Accuracy (Validation)")

### Main Loop
for (lcv in seq_along(adjList)) {

  gname <- names(adjList)[lcv]
  cat("\n\nBuilding LogMiNeR Regression Model for", gname, "Model...\n")

  cvList <- vector(mode="list", length=length(alphas))
  names(cvList) <- as.character(alphas)
  for (i in seq_along(alphas)) {
    if (gname == "Unconnected") {
      ## Unconnected network only makes sense when alpha == 1 since adjacency
      ## matrix is all zeros
      if (alphas[i] == 1) {
        adjMat <- NULL
      } else {
          next
        }
    } else {
        adjMat <- as.matrix(adjList[[lcv]])
      }
    if (verbose) {
      cat("alpha =", alphas[i], ':\n')
    }
    cvList[[i]] <- cv.pclogit(x = X_train, y = as.factor(Y_train), foldid = foldid,
                              alpha = alphas[i], nlam = nlam, corDir = corDir,
                              grouped = TRUE, group = adjMat, type.measure = "class",
                              parallel = parallelFlag)
  }
  cvList <- cvList[! vapply(cvList, is.null, logical(1))]

  ## Find the alpha value that has the minimum cross validation measure
  ## If multiple alpha values share the same minimum, choose the more sparse one
  cvmMat <- Reduce(cbind, lapply(cvList, function(cvRes)
    if (is.null(cvRes)) {
      return(NA)
    } else {
        return(as.matrix(cvRes$cvm))
      }))
  cvsdMat <- Reduce(cbind, lapply(cvList, function(cvRes)
    if (is.null(cvRes)) {
      return(NA)
    } else {
        return(cvRes$cvsd)
      }))
  allInds <- which(cvmMat == min(cvmMat, na.rm=TRUE), arr.ind=TRUE)
  ## Include any models within one standard error of the minimum
  if (cvMeth == "lambda.1se") {
    se <- min(cvsdMat[allInds], na.rm = TRUE)
    allInds <- which(cvmMat < (min(cvmMat, na.rm=TRUE) + se), arr.ind=TRUE)  
  }
  ## Choose most sparse or least sparse depending on the value of cvOpt
  if (cvOpt == "sparse") {
    rind <- nrow(allInds)
  } else if (cvOpt == "net") {
    rind <- 1
  }
  sel <- allInds[rind, "col"]
  pclogit.cv.result <- cvList[[sel]]

  ## Training (Fitted) Confusion Matrix
  preds <- predict(pclogit.cv.result, X_train, s=cvMeth, type="class", grouped=FALSE)
  lisTrain <- list(predicted=factor(preds, levels=0:1),
                   true=factor(as.numeric(Y_train), levels=0:1))
  tabTrain <- table(predicted=lisTrain$predicted, true=lisTrain$true)
  if (interactive() || sinkOut) {
    printMissed(l=lisTrain, subset=which(train), dat=targetTrain,
                cols = targetCols,
                title="Training (Fitted) Model")
    printConfusionResults(tabTrain)
  }

  if (exists("X_test")) {
    ## Testing Confusion Matrix
    preds <- predict(pclogit.cv.result, X_test, s=cvMeth, type="class")
    lisTest <- list(predicted=factor(preds, levels=0:1),
                    true=factor(as.numeric(Y_test), levels=0:1))
    tabTest <- table(predicted=lisTest$predicted, true=lisTest$true)
    if (interactive() || sinkOut) {
      printMissed(l=lisTest, subset=which(test), dat=targetTest,
                  cols = targetCols,                  
                  title="Testing Model")
      printConfusionResults(tabTest)  
    }
  } else {
      tabTest <- as.table(matrix(rep(NA, 4), nrow=2,
                                 dimnames=list(predicted = c("0", "1"),
                                     true = c("0", "1"))))
    }
  
  ## Store results
  numSelGenes <- pclogit.cv.result$glmnet.fit$df[which(pclogit.cv.result[[cvMeth]] ==
                                                            pclogit.cv.result$lambda)]
  weights <- pclogit.cv.result$glmnet.fit$beta[,which(pclogit.cv.result[[cvMeth]] ==
                                                          pclogit.cv.result$lambda)]
  cvErrorOpt <- pclogit.cv.result$cvm[pclogit.cv.result$lambda ==
                                          pclogit.cv.result[[cvMeth]]]
  cvSEOpt <- pclogit.cv.result$cvsd[pclogit.cv.result$lambda ==
                                          pclogit.cv.result[[cvMeth]]]
  wList[[gname]] <- weights[order(names(weights))]
  modList[[gname]] <- pclogit.cv.result
  ## Store results in a dataframe
  resultTable[lcv,] <- c(gname, nrow(adjList[[gname]]),
                         cvErrorOpt, cvSEOpt,
                         pclogit.cv.result$glmnet.fit$alpha,
                         pclogit.cv.result[[cvMeth]],
                         numSelGenes,
                         Sensitivity(tabTrain), Specificity(tabTrain), Accuracy(tabTrain),
                         Sensitivity(tabTest), Specificity(tabTest), Accuracy(tabTest))

  cat("Results:\n\n\n")
  print(resultTable[lcv,])
  
  cat(paste0(lcv, '/', length(adjList),' model selection and evaulations completed.\n'))
  
} # end for loop over adjList (indexed by lcv)

resultList$logminer <- resultTable
modelList$logminer <- modList
weightList$logminer <- wList

######################################################################################
## Write out result table and save weights as R Data object                         ##
######################################################################################
if (sinkOut) {
  cat('\n\nFINAL RESULTS\n\n')
  show(resultList)
  cat('\n\nSession Info:\n\n')
  capture.output(sessionInfo(), file = sinkFile, append = TRUE)
}

outputTable <- rbindlist(resultList, fill = TRUE)
modelNames <- rep(names(resultList), vapply(resultList, nrow, FUN.VALUE = numeric(1)))
outputTable$Model <- modelNames
write.table(outputTable, quote = FALSE, sep = '\t', row.names = FALSE,
            file = file.path(OUTPUT_DIR, paste0("ClassificationResults_", tag, ".txt")))
saveRDS(resultList, file = file.path(OUTPUT_DIR, paste0("resultList_", tag, '.rds')))
saveRDS(modelList, file = file.path(OUTPUT_DIR, paste0("modelList_", tag, '.rds')))
saveRDS(weightList, file = file.path(OUTPUT_DIR, paste0("weightList_", tag, '.rds')))

