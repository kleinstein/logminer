#! /bin/bash

###############################################################################
# Example shell script used to submit LogMiNeR jobs in batch to an HPC        #
# This script is called by master_HPC.R					      #
###############################################################################
## Note this command is customized for a particular HPC and should be adapted
## depending on particular queuing system.
sbatch -o ~/tmp/slurm-%j.out -p pi_kleinstein -J LogMiNeR -c 4 --mem=25000 -n 1 -C m915 --time=2:00:00 --mail-type=ALL ./logminer.R $1 $2 $3 $4

# sbatch -o ~/tmp/slurm-%j.out -p pi_kleinstein -J LogMiNeR-LARGE -c 12 --mem=30000 -n 1 -C m915 --time=24:00:00 --mail-type=ALL ./logminer.R $1 $2 $3 $4
