# LogMiNeR: Logistic Multiple Network-constrained Regression #


[TOC]


LogMiNeR is a framework for systems biology studies that fits multiple models, each using different sources of prior knowledge, to gain a broader understanding of the underlying biology. The case studies included here use transcriptional profiling data from gene expression microarrays to build a predictive model of immune perturbation or disease state.

## Publication ##

### Motivation ###
Systems immunology leverages recent technological advancements that enable broad profiling of the immune system to better understand the response to infection and vaccination, as well as the dysregulation that occurs in disease. An increasingly common approach to gain insights from these large-scale profiling experiments involves the application of statistical learning methods to predict disease states or the immune response to perturbations. However, the goal of many systems studies is not to maximize accuracy, but rather to gain biological insights. The predictors identified using current
approaches can be uninterpretable or present only one of many equally predictive models, leading to a narrow understanding of the underlying biology.

### Results ###
Here we show that incorporating prior biological knowledge within a logistic modeling framework by using network-level constraints on transcriptional profiling data significantly improves interpretability. Moreover, incorporating different types of biological knowledge produces models that highlight distinct aspects of the underlying biology, while maintaining predictive accuracy. We propose a new framework, Logistic Multiple Network-constrained Regression (LogMiNeR), and apply it to understand the mechanisms underlying differential responses to influenza vaccination. While standard logistic regression approaches were predictive, they were minimally interpretable. Incorporating prior knowledge using LogMiNeR led to models that were equally predictive yet highly interpretable. In this context, B cell-specific genes and mTOR signaling were associated with an effective vaccination response in young adults. Overall, our results demonstrate a new paradigm for analyzing high-dimensional immune profiling data in which multiple networks encoding prior knowledge are incorporated to improve model interpretability.

### Contact ###

- [stefan.avey@yale.edu](mailto::stefan.avey@yale.edu?subject="LogMiNeR Code")
- [steven.kleinstein@yale.edu](mailto::steven.kleinstein@yale.edu?subject="LogMiNeR Code")

### Citation ###

- Avey S, et al. (2017) Multiple network-constrained regressions expand insights into influenza vaccination responses. Bioinformatics. *In Press*.

## Dependencies ##

The following R packages must be installed before using LogMiNeR. **Note: `pclogit` (and therefore LogMiNeR) can only be installed in a *nix environment.** `pclogit` was developed by [Hokeun Sun](http://statsun.pusan.ac.kr/) and the source is redistributed here with the author's permission.

Run the following commands from the R prompt to install the R package dependencies.

```
#!R
## Required packages for LogMiNeR
install.packages(c("igraph", "glmnet", "data.table", "yaml", "parallel", "doMC", "devtools"))
install_url("https://bitbucket.org/kleinstein/logminer/downloads/pclogit_0.2.tar.gz")

## Required pacakges for figures
install.packages(c("ggplot2" "dplyr" "tidyr"  "RColorBrewer" "sets" "NMF" "openxlsx" "pROC"))
source("https://bioconductor.org/biocLite.R")
biocLite(c("limma" "RCytoscape"))
```

## Getting Started ##

LogMiNeR is written in R and can be run from any *nix environment from the command line. To get started, make sure the dependencies above are installed and then:

1. Clone or [download](https://bitbucket.org/kleinstein/logminer/get/tip.zip) this repository to your local machine.
2. [Download](http://clip.med.yale.edu/papers/Avey2017BINF/Data.tar.gz) and uncompress the data archive (`Data.tar.gz` ~700 Mb)
3. Test the code by following the [Test Run](#markdown-header-test-run) below.

### Arguments ###

LogMiNeR requires at least 4 inputs (supplied as command line arguments in this order).

1. **Data Directory**: The path to the (extracted) data directory
2. **Configuration file**: The YAML formatted config file specifying parameter values
3. **Output Directory**: The path to write output files to
4. **Number of iterations**: A positive integer specifying the number of iterations of cross validation
5. **Iteration Start Number**: (Optional) A positive integer specifying what the first iteration should be. This is useful for running more iterations after some have already been run (e.g. if 5 iterations have already completed, supply 6 for the iteration start number). Defaults to 1 and any previously generated files will be overwritten without warning.

### Test Run ###

To test that everything is correctly installed, you can run the included test run.  Launch a terminal window and run the following commands replacing `path/to/` with the correct path to the directory

```
#!bash

## Change working directory to Code subdirectory of this repository
cd path/to/logminer/Code

## Run the provided test
tar -xzvf path/to/Data.tar.gz
./master.R path/to/Data ../Output/test/config_test.yml ../Output/test 5
## Wait until all 5 iterations have completed by monitoring output directory
## then create figures:
./figures.R path/to/Data ../Output/test/config_test.yml ../Output/test 5
```

This command will run LogMiNeR on a small subset of genes in the influenza vaccination data.  5 cross validation iterations are performed. Note that these results are **not** meant to be biologically useful because few genes are used as features. This is only meant to test that the software runs correctly on a small number of genes before scaling up to the full model.

## Reproducing Paper Results ##

### Running LogMiNeR ###

From inside the Code subdirectory of this repository, run one of the following lines

**NOTE**: 50 iterations were run to produce the results in the paper but this will use a considerable ammount of memory and resources since each iteration is a separate job that itself uses multi-threading. To run the full model, use [*master_HPC.R*](https://bitbucket.org/kleinstein/logminer/src/tip/Code/master_HPC.R) in place of *master.R* and modify [*submitJob.sh*](https://bitbucket.org/kleinstein/logminer/src/tip/Code/submitJob.sh) to work for your HPC.

```
#!bash

## Flu
./master.R path/to/data ../Output/FLU/config_FLU.yml ../Output/FLU 5

## GSE45291
./master.R path/to/data ../Output/GSE45291/config_GSE45291.yml ../Output/GSE45291 5

## GSE37250
./master.R path/to/data ../Output/GSE37250/config_GSE37250.yml ../Output/GSE37250 5

## GSE57338
./master.R path/to/data ../Output/GSE57338/config_GSE57338.yml ../Output/GSE57338 5

```

You can check on the status of the jobs by changing to the output directory and viewing the Sink file (1 for each iteration).

### Creating Figures ###

Figures can only be created once all iterations have completed.  You can check the output directory to make sure that a file starting with "ClassificationResults" exists for each iteration. From inside the Code subdirectory of this repository, run the following line corresponding to the dataset used.

```
#!bash

## Flu
./figures.R path/to/data ../Output/FLU/config_FLU.yml ../Output/FLU 5

## GSE45291
./figures.R path/to/data ../Output/GSE45291/config_GSE45291.yml ../Output/GSE45291 5

## GSE37250
./figures.R path/to/data ../Output/GSE37250/config_GSE37250.yml ../Output/GSE37250 5

## GSE57338
./figures.R path/to/data ../Output/GSE57338/config_GSE57338.yml ../Output/GSE57338 5

```


## Modifying The Configuration ##

The YAML formatted configuration file specifies all parameter values. These can be modified to play with different settings on these datasets or specify different datasets. 
Note that not all parameter combinations have been tested and only the parameter combinations in the supplied config files are known to work.

- [*Output/test/config\_test.yml*](https://bitbucket.org/kleinstein/logminer/src/tip/Output/test/config_test.yml)
- [*Output/FLU/config\_FLU.yml*](https://bitbucket.org/kleinstein/logminer/src/tip/Output/FLU/config_FLU.yml)
- [*Output/GSE45291/config\_GSE45291.yml*](https://bitbucket.org/kleinstein/logminer/src/tip/Output/GSE45291/config_GSE45291.yml)
- [*Output/GSE37250/config\_GSE37250.yml*](https://bitbucket.org/kleinstein/logminer/src/tip/Output/GSE37250/config_GSE37250.yml)
- [*Output/GSE57338/config\_GSE57338.yml*](https://bitbucket.org/kleinstein/logminer/src/tip/Output/GSE57338/config_GSE57338.yml)

### Configuration Options ###

#### Options ####

- **sink**: logical value specifying whether R output will be diverted to a file in the output directory.
- **verbose**: logical value specifying whether to print extra tracing and debugging information (currently not very useful)
- **parallel**: logical value specifying whether to register a multi-core backend for parallelizing the cross validation procedure.

#### Data ####

- **networks**: Name of an RDS (R Data Set) file in the data directory containing an igraph object with the networks to use for regularization (not currently customizable).
- **mode**: FLU or GEO. If FLU, additional balancing and scaling of the data is done. If using your own data, use mode GEO.
- **accession**: (optional) name to specify what GEO (or other) dataset is being used. Will be added to the output file tag.
- **time**: Number indicating which timepoint (in the `Time` column) from the data should be used. Use NULL if no filtering by time should be done.  If the time is not 0, the value will be subtracted from the value at time 0.
- **response**:
    - **type**: Name of the pheno column giving the two values to compare
    - **low**: Character string specifying which samples are in the low or control group
    - **high**: Character string specifying which samples are in the high or case group
- **train**:
    - **expr**: Name of a TSV file in the data directory containing the expression data for model training. Features must be in rows (first column is the feature name) and observations in columns (first row contains the observation names)
    - **pheno**: Name of a TSV file in the data directory containing the phenotype data for model training. Observations should be in rows (first column is the observation name which maps to the column names of expr) and attributes in column. Required attributes are: `SubjectID`, `Time`, and the value specified in data.response.type.
- **test**:
    - **expr**: Name of a TSV file in the data directory containing the expression data for model testing. Must be formatted in the same way as data.train.expr
    - **pheno**: Name of a TSV file in the data directory containing the phenotype data for model testing. Must be formatted in the same way as data.train.pheno
  
#### Feature Selection ####

- **method**: The name of a feature selection method. One of AvgAb (Average of the Absolute Value), CV (Coefficient of Variation).
- **numFeatures**: Number of features to select from ranking by the featureSelection.method


#### Cross Validation  ####

- **method**: The criteria to use for optimizing the tuning parameters. lambda.min to choose the parameters that minimize the prediction error or lambda.1se to choose the parameters that produce the crossValidation.best model within 1 standard error of the minimum (preferred).
- **best**: What to prefer if multiple models have the same error or are within 1 standard error of the minimum. `sparse` selects the most sparse (most L1 penalized) model in all cases whereas `net` prefers more L2 or network constraint but does not effect the Lasso models which always use the most sparse model.
- **numFolds**: Number of folds for cross validation
- **numLambda**: Number of lambda values to test
- **numAlpha**: Number of alpha values to test

#### LogMiNeR ####

- **corDir**: Logical value specifying whether the adaptive pclogit procedure should be used with coefficient signs estimated from correlation with response during cross validation
- **permute**: Logical value specifying whether permuted versions of each network should be used in addition to original networks.
